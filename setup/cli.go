package setup

import (
	"drummachinesequencer/config"
	"drummachinesequencer/drum"
	"drummachinesequencer/inputformat"
	"drummachinesequencer/patch"
	"drummachinesequencer/patch/beat"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"time"
)

// CLI object for common drum machine instantiation
type CLI struct {
	dkit   *beat.Beat
	patch  *patch.Patch
	done   chan os.Signal
	bpm    int
	name   string
	ticker *time.Ticker
}

// New CLI for use in the drum machine
func New() *CLI {
	return &CLI{
		dkit:   new(beat.Beat),
		patch:  new(patch.Patch),
		done:   make(chan os.Signal, 1),
		bpm:    60,
		name:   "Temporary Name",
		ticker: new(time.Ticker),
	}
}

func (c *CLI) loadDrumkit(path string) {
	dkitinfo, err := inputformat.Deserialize(path, ioutil.ReadFile)
	if err != nil {
		panic(err)
	}
	client := inputformat.Client{}
	c.dkit = client.LoadDrumkit(&inputformat.Adapter{Sounds: dkitinfo})
}
func (c *CLI) loadPatch(path string) {
	config := config.Deserialize(path)
	c.name = config.Name
	if config.BPM > 0 {
		c.bpm = config.BPM
	}
	c.patch = patch.New()
	for _, configBeat := range config.Patch {
		b := beat.New()
		for idx, val := range configBeat {
			if val == 1 {
				b.Add(c.dkit.Get(idx))
			}
		}
		c.patch.Add(b)
	}
}

// Play is called to run the implementation of a drum machine
// you know ntz ntz swoosh ntz ;)
func (c *CLI) Play() {
	c.ticker = drum.New(c.bpm)
	signal.Notify(c.done, os.Interrupt)
	fmt.Printf("Song Name: %s\n", c.name)
	fmt.Printf("Playing song at BPM: %d\n", c.bpm)
	fmt.Println("Press Ctrl+C to Quit")
	go func() {
		count := 0
		fmt.Printf("|")
		for {
			select {
			case <-c.done:
				return
			case <-c.ticker.C:
				{
					err := c.patch.Play()
					if err != nil {
						log.Printf("WARN: %s", err.Error())
					}
					fmt.Printf("|")
					count++
					if count%8 == 0 {
						fmt.Println()
						fmt.Printf("|")
					}
				}
			}
		}
	}()
	<-c.done
	return
}
