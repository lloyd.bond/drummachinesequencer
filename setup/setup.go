package setup

type iTemplate interface {
	loadDrumkit(string)
	loadPatch(string)
	Play()
}

// AbstractSetup struct for implementing the Template Method design pattern
type AbstractSetup struct {
	ITemplate iTemplate
}

// SetupAndStart will automatically call your implemented versions of methods for iTemplate and start execution
func (s *AbstractSetup) SetupAndStart(configPath, dkitPath string) {
	s.ITemplate.loadDrumkit(dkitPath)
	s.ITemplate.loadPatch(configPath)
	s.ITemplate.Play()
	return
}
