# DRUM MACHINE SEQUENCER

This program simulates a drum machine in the console with text output. Here is an example of what you should expect.

```
    2020/03/25 08:41:05 starting application ....  
    Song Name: Four on the Floor Basic Drum Kit  
    Playing song at BPM: 300  
    Press Ctrl+C to Quit  
    |Bass Drum|_|OpenHiHat|_|Bass DrumSnare Drum|_|OpenHiHat|_|  
    |Bass Drum|_|OpenHiHat|_|Bass DrumSnare Drum|_|OpenHiHat|_|  
    |Bass Drum|_|OpenHiHat|_|Bass DrumSnare Drum|_|OpenHiHat|_|  
    |Bass Drum|_|OpenHiHat|_|Bass DrumSnare Drum|_|OpenHiHat|_|  
    |Bass Drum|_|OpenHiHat|_|Bass DrumSnare Drum|_|OpenHiHat|_|  
    |Bass Drum|_|OpenHiHat|^C  
    2020/03/25 08:41:14 quiting application ....  
```
## GETTING THE SOURCE

Clone or download the source from [gitlab.com here](https://gitlab.com/lloyd.bond/drummachinesequencer)

## BUILDING

### Docker

The easiest way to build is with docker if you have that installed.
>`docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.14 go build -v`

If you need to compile for windows or mac try the following

>for windows x86_64

>>`docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp -e GOOS=windows -e GOARCH=amd64 golang:1.14 go build -v`

>for Mac x86_64
>>`docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp -e GOOS=darwin -e GOARCH=amd64 golang:1.14 go build -v`

Check the golang documentation for other supported OS/ISA compiler supported options

### Local Build Environment

else, you can setup a go environment locally with instructions from here [GolangTools](https://www.golang.org)

>then you can build with
>>`go build -v`

>for windows x86_64
>>`GOOS=windows GOARCH=amd64 go build -v -o drummmachine-$GOOS-$GOARCH`

>for mac x86_64
>>`GOOS=darwin GOARCH=amd64 go build -v -o drummmachine-$GOOS-$GOARCH`

## RUNNING

to run the cli drumkit, you need to supply config for the drumkit and information about your sound files. both in json format

as a sample this will demo the drummachine to you

`./drummachinesequencer -c basic_config.json -d audio/basic_drum_kit.json`

+ -c lets you import the config for your song
    - sample  config
        ```
        {
            "name":"Four on the Floor Basic Drum Kit",
            "bpm":300,
            "patch":[
                [0,1,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,1,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,1,1,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,1,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,1,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,1,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,1,1,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,1,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0]
            ]
        }
        ```
+ -d tells the drummachine where your sound files and information are for your drumkit
    - sample drum kit
        ```
        [
            {"name":"song name1","file": "path/file1.wav",	"description":"song name1 description"},
            {"name":"song name2","file": "path/file2.wav",	"description":"song name2 description"}          
        ]
        ```




*Special note, while audio files are included, there is no sound support yet*

## TESTING

To run the unit tests

### Docker

>`docker run --rm -it -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.14 bash`

from the docker shell
>`make test -v ./...`

### Local Build Environment

`make test -v ./...`
