package config

import (
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"testing"
)

func TestDeserialize_Path_Empty(t *testing.T) {
	path := ""
	expected := &Config{
		Name: "Four on the Floor",
		BPM:  59,
		Patch: [][]int{
			{0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
	}
	actual := Deserialize(path)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFail: Deserialize(%s)\nExpected:%v\nGot:%v", path, expected, actual)
	}
}
func TestDeserialize_Path_InValid(t *testing.T) {
	path := "audio/not.valid"
	expected := &Config{
		Name: "Four on the Floor",
		BPM:  59,
		Patch: [][]int{
			{0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
	}
	actual := Deserialize(path)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFail: Deserialize(%s)\nExpected:%v\nGot:%v", path, expected, actual)
	}
}
func TestDeserialize_Path_Valid(t *testing.T) {
	file := createTempFile("valid.json")
	defer file.Close()
	file.WriteString(`{"name":"test song","bpm":300,"patch":[[0,1,0,0,1],[0,1,0,0,1],[0,1,0,0,1]]}`)
	path := file.Name()
	expected := &Config{
		Name: "test song",
		BPM:  300,
		Patch: [][]int{
			{0, 1, 0, 0, 1},
			{0, 1, 0, 0, 1},
			{0, 1, 0, 0, 1},
		},
	}
	actual := Deserialize(path)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFail: Deserialize(%s)\nExpected:%v\nGot:%v", path, expected, actual)
	}
}

func createTempFile(fname string) *os.File {
	file, err := ioutil.TempFile("", fname)
	if err != nil {
		log.Fatal(err)
	}
	return file
}
