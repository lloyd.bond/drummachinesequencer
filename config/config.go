package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// Config object for your tracks
type Config struct {
	Name  string  `json:"name"`
	BPM   int     `json:"bpm"`
	Patch [][]int `json:"patch"`
}

var defaultData string = `{"name":"Four on the Floor","bpm":59,"patch":[[0,1,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,1,0],[0,0,0,0,0,0,0,0,0,0],[0,1,1,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,1,0],[0,0,0,0,0,0,0,0,0,0],[0,1,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,1,0],[0,0,0,0,0,0,0,0,0,0],[0,1,1,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,1,0],[0,0,0,0,0,0,0,0,0,0]]}`

// Deserialize your config file
func Deserialize(path string) *Config {
	c := Config{}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("failed to load config file at (%s)", path)
		data = []byte(defaultData)
	}
	err = json.Unmarshal(data, &c)
	if err != nil {
		log.Fatalf("failed to deserialize json data at (%s) with error : %v", path, err.Error())
	}

	return &c
}
