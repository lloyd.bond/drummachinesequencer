// Here is a drummer to calculate and generate a timer for your rythm.

package drum

import (
	"time"
)

const nanosecconversion = 60000000000

// New sets the tempo in the drum machine based on a the given BPM
func New(bpm int) *time.Ticker {
	return time.NewTicker(calculateSeconds(bpm))
}

// CalculateSeconds duration between beats
func calculateSeconds(bpm int) time.Duration {
	if bpm < 1 || bpm > nanosecconversion {
		return time.Duration(0)
	}
	return time.Duration(nanosecconversion / bpm)
}
