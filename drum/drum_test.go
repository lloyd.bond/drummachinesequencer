package drum

import (
	"testing"
)

const MaxInt = int(^uint(0) >> 1)
const MinInt = -MaxInt - 1

var bpmTests = []struct {
	input       int
	expected    int64
	description string
}{
	{0, 0, "bpm input test the zero case"},
	{10, 6000000000, "bpm input test an unusually small case"},
	{-128, 0, "bpm input test a negative case"},
	{128, 468750000, "bpm input test a normal case"},
	{300, 200000000, "bpm input test an unusually large case"},
	{MaxInt, 0, "bpm input test the max int case"},
	{MinInt, 0, "bpm input test the min int case"},
}

func TestCalculateSeconds_BPM(t *testing.T) {

	for _, test := range bpmTests {
		actual := calculateSeconds(test.input)
		if actual.Nanoseconds() != test.expected {
			t.Fatalf("FAIL: %s CalculateSeconds(%d) = %d, want %q.", test.description, test.input, actual, test.expected)
		}
		t.Logf("PASS: %s", test.description)
	}
}

func BenchmarkCalculateSeconds(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, test := range bpmTests {
			calculateSeconds(test.input)
		}
	}
}
