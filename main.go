package main

import (
	"drummachinesequencer/setup"
	"flag"
	"fmt"
	"log"
)

func main() {
	log.Println("starting application ....")
	configPath := flag.String("c", "./config.json", "path to your ")
	dkitPath := flag.String("d", "./audio/basic_drum_kit.json", "path to your drum kit's sound information")
	flag.Parse()

	abstractSetup := setup.AbstractSetup{ITemplate: setup.New()}

	abstractSetup.SetupAndStart(*configPath, *dkitPath)

	fmt.Println()
	log.Println("quitting application ....")
}
