package sound

import "os"

// Sound is an item used to track all the information related to a sound
type Sound struct {
	name        string
	file        string
	description string
}

// dependency injection used later for testing
type fileExistCallback func(name string) (os.FileInfo, error)

//New allocatessound object and does some boiler plate checking
func New(name, file, description string, exists fileExistCallback) (*Sound, error) {
	_, err := exists(file)
	if err != nil {
		return nil, err
	}

	return &Sound{
		name,
		file,
		description,
	}, nil
}

// String what's in the sound object, right now it's just the name
func (s *Sound) String() string {
	return s.name
}

// Name getter
func (s *Sound) Name() string {
	return s.name
}

// Description getter
func (s *Sound) Description() string {
	return s.description
}

// File path getter
func (s *Sound) File() string {
	return s.file
}
