package sound

import (
	"fmt"
	"os"
	"reflect"
	"testing"
)

func MockFileExistsCB(name string) (os.FileInfo, error) {
	return nil, nil
}

func TestNew_BadOrUnaccessibleFile(t *testing.T) {
	fakeName, fakeFile, fakeDesc := "Song Name", "audio/crash.wav", "drum instrument crash"

	_, err := New(fakeName, fakeFile, fakeDesc, func(name string) (os.FileInfo, error) {
		return nil, fmt.Errorf("file (%s) does not exist or is unaccessible", name)
	})
	expected := fmt.Errorf("file (%s) does not exist or is unaccessible", fakeFile)
	if err.Error() != expected.Error() {
		t.Fatalf("\nFAIL: New(%s,%s,%s)\nExpected : %s\nGot : %s", fakeName, fakeFile, fakeDesc, expected.Error(), err.Error())
	}

}

func TestNew_Success(t *testing.T) {
	fakeName, fakeFile, fakeDesc := "Song Name", "audio/crash.wav", "drum instrument crash"
	expected := &Sound{
		name:        fakeName,
		file:        fakeFile,
		description: fakeDesc,
	}

	actual, err := New(fakeName, fakeFile, fakeDesc, MockFileExistsCB)

	if err != nil {
		t.Fatalf("\nFAIL: New(%s,%s,%s)\nExpected: error to be nil\nGot %s", fakeName, fakeFile, fakeDesc, err.Error())
	}

	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: New(%s,%s,%s)\nExpected : %v\nGot : %v", fakeName, fakeFile, fakeDesc, expected, actual)
	}
}
