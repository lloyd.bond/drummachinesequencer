package patch

import (
	"drummachinesequencer/patch/beat"
	"fmt"
)

// Patch is a drum machine's collection of
// drum sounds to play for one measure
type Patch struct {
	beats []*beat.Beat
	hash  map[*beat.Beat]int
	curr  int
}

// New intializes sounds for each beat in a track
func New(beats ...*beat.Beat) *Patch {
	p := &Patch{
		beats: make([]*beat.Beat, 0),
		hash:  make(map[*beat.Beat]int),
		curr:  -1,
	}

	for _, beat := range beats {
		p.Add(beat)
	}
	return p
}

// Add a set of sounds for a beat in a track
func (p *Patch) Add(beat *beat.Beat) {
	if beat == nil {
		return
	}
	if p.curr == -1 {
		p.curr = 0
	}
	p.beats = append(p.beats, beat)
	p.hash[beat] = p.Len() - 1

}

// Insert does an insert to the your track
func (p *Patch) Insert(bt *beat.Beat, pos int) int {
	if !p.insertable(pos) || bt == nil {
		return -1
	}
	tmp := append([]*beat.Beat{bt}, p.beats[pos:]...)
	p.beats = append(p.beats[:pos], tmp...)
	p.recalculateHash()
	if p.curr >= pos {
		p.curr++
	}
	return 0
}

// Move does a remove then an insert into your patch
func (p *Patch) Move(bt *beat.Beat, pos int) int {
	if !p.insertable(pos) || bt == nil {
		return -1
	}
	p.Remove(bt)
	tmp := append([]*beat.Beat{bt}, p.beats[pos:]...)
	p.beats = append(p.beats[:pos], tmp...)
	p.recalculateHash()
	return 0
}

// Remove a beat from the track
func (p *Patch) Remove(beat *beat.Beat) {
	if beat == nil || p.Len() < 1 {
		return
	}
	pos := p.hash[beat]
	p.beats = append(p.beats[:pos], p.beats[pos+1:]...)
	delete(p.hash, beat)
	p.recalculateHash()
	if p.curr >= pos {
		if p.curr > 0 {
			p.curr--
		}
	}
}

// Play method will call the action that shows or sounds the play of a beat
func (p *Patch) Play() error {
	if !p.hasNext() {
		return fmt.Errorf("no beats to play")
	}
	defer p.next()
	beat := p.current()
	beat.Play()
	return nil
}

// Len returns the element count of a track
func (p *Patch) Len() int {
	return len(p.beats)
}

func (p *Patch) recalculateHash() {
	for idx, b := range p.beats {
		p.hash[b] = idx
	}
}

func (p *Patch) insertable(pos int) bool {
	return pos >= 0 && pos < p.Len()
}

func (p *Patch) hasNext() bool {
	return p.Len() > 0
}

func (p *Patch) next() {
	p.curr++
	if p.curr >= p.Len() {
		p.curr = 0
	}
}

func (p *Patch) current() *beat.Beat {
	return p.beats[p.curr]
}
