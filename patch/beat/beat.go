package beat

import (
	"drummachinesequencer/patch/sound"
	"fmt"
	"log"
	"strings"
)

// Beat object keeps track of a list of sounds to be played on that specific beat in the patch/track/hypermeasure
type Beat struct {
	sounds []*sound.Sound
	hash   map[*sound.Sound]int
	mixed  string
}

// New given a list of sounds, setup a Beat object to be used in the drum machine
func New(sounds ...*sound.Sound) *Beat {
	b := &Beat{
		sounds: make([]*sound.Sound, 0),
		hash:   make(map[*sound.Sound]int),
		mixed:  "_",
	}
	defer b.preProcessBeat()
	for _, sound := range sounds {
		if sound == nil {
			log.Println("omitting from beat table 'nil' value found in song list")
			continue
		}
		b.sounds = append(b.sounds, sound)
		b.hash[sound] = len(b.sounds) - 1
	}
	for i, sound := range b.sounds {
		b.hash[sound] = i
	}
	return b
}

// Add a sound from your drumkit to beat
func (b *Beat) Add(sound *sound.Sound) {
	if sound == nil {
		log.Println("bypassing attempt to add 'nil' sound")
		return
	}
	defer b.preProcessBeat()
	b.sounds = append(b.sounds, sound)
	b.hash[sound] = b.Len() - 1

}

// Remove a sound from your beat
func (b *Beat) Remove(sound *sound.Sound) {
	if sound == nil {
		log.Println("bypassing attempt to remove 'nil' sound")
		return
	}
	if len(b.sounds) < 1 {
		log.Println("beat already empty, nothing to remove")
		return
	}
	defer b.preProcessBeat()
	pos := b.hash[sound]
	delete(b.hash, sound)
	b.sounds = append(b.sounds[:pos], b.sounds[pos+1:]...)
	if pos+1 > len(b.sounds) {
		return
	}
	for idx, snd := range b.sounds[pos:] {
		b.hash[snd] = pos + idx
	}
}

// Play executes the action to play
func (b *Beat) Play() {
	fmt.Printf(b.mixed) // for cli we just need text output
}

// Get the sound addres by index, helpful for a UI
func (b *Beat) Get(k int) *sound.Sound {
	if k < 0 || k >= b.Len() {
		return nil
	}
	return b.sounds[k]
}

// Len returns the length of the Beat structure
func (b *Beat) Len() int {
	return len(b.sounds)
}

func (b *Beat) preProcessBeat() {
	var sb strings.Builder
	for _, snd := range b.sounds {
		sb.WriteString(snd.String())
	}
	if sb.Len() < 1 {
		sb.WriteRune('_')
	}
	b.mixed = sb.String()
}
