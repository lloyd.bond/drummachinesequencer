package beat

import (
	"drummachinesequencer/patch/sound"
	"os"
	"reflect"
	"testing"
)

func MockFileExistsCB(name string) (os.FileInfo, error) {
	return nil, nil
}

func TestNew_EmptySoundList(t *testing.T) {

	expected := &Beat{
		sounds: make([]*sound.Sound, 0),
		hash:   make(map[*sound.Sound]int),
		mixed:  "_",
	}
	actual := New([]*sound.Sound{}...)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New([]*sound.Sound{})\nExpected:%v\nGot:%v", expected, actual)
	}

}

func TestNew_EmptySoundListParameter(t *testing.T) {

	expected := &Beat{
		sounds: make([]*sound.Sound, 0),
		hash:   make(map[*sound.Sound]int),
		mixed:  "_",
	}
	actual := New()
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New()\nExpected:%v\nGot:%v", expected, actual)
	}

}

func TestNew_NilSoundList(t *testing.T) {
	expected := &Beat{
		sounds: make([]*sound.Sound, 0),
		hash:   make(map[*sound.Sound]int),
		mixed:  "_",
	}
	actual := New(nil)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(nil)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestNew_WithSoundList(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd1, snd2},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1},
		mixed:  "crashcrash2",
	}

	actual := New(snd1, snd2)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(two sounds)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestAdd_Nil(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd1, snd2},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1},
		mixed:  "crashcrash2",
	}

	actual := New(snd1, snd2)
	actual.Add(nil)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(two sounds)\nExpected:%v\nGot:%v", expected, actual)
	}
}
func TestAdd_Sound(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd1, snd2},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1},
		mixed:  "crashcrash2",
	}

	actual := &Beat{
		sounds: []*sound.Sound{snd1},
		hash:   map[*sound.Sound]int{snd1: 0},
		mixed:  "crash",
	}
	actual.Add(snd2)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(two sounds)\nExpected:%v\nGot:%v", expected, actual)
	}
}

// func TestAdd_Sound_WhenEmpty(t *testing.T)

func TestRemove_Nil(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd1, snd2},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1},
		mixed:  "crashcrash2",
	}

	actual := New(snd1, snd2)
	actual.Remove(nil)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(two sounds)\nExpected:%v\nGot:%v", expected, actual)
	}
}
func TestRemove_Sound_FromBeggining(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd2: 0, snd3: 1, snd4: 2},
		mixed:  "crash2crash3crash4",
	}

	actual := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2, snd4: 3},
		mixed:  "crashcrash2crash3crash4",
	}
	actual.Remove(snd1)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: Remove(crash)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestRemove_Sound_FromEnd(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2},
		mixed:  "crashcrash2crash3",
	}

	actual := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2, snd4: 3},
		mixed:  "crashcrash2crash3crash4",
	}
	actual.Remove(snd4)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(two sounds)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestRemove_Sound_FromMiddle(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{snd1, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd3: 1, snd4: 2},
		mixed:  "crashcrash3crash4",
	}

	actual := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2, snd4: 3},
		mixed:  "crashcrash2crash3crash4",
	}
	actual.Remove(snd2)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: New(two sounds)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestRemove_Sound_WhenEmpty(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{},
		hash:   map[*sound.Sound]int{},
		mixed:  "_",
	}

	actual := &Beat{
		sounds: []*sound.Sound{},
		hash:   map[*sound.Sound]int{},
		mixed:  "_",
	}
	actual.Remove(snd1)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: Remove(crash)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestRemove_Sound_WhenOne(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	expected := &Beat{
		sounds: []*sound.Sound{},
		hash:   map[*sound.Sound]int{},
		mixed:  "_",
	}

	actual := &Beat{
		sounds: []*sound.Sound{snd1},
		hash:   map[*sound.Sound]int{snd1: 0},
		mixed:  "_",
	}
	actual.Remove(snd1)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: Remove(crash)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestPreProcess_Beat_WhenEmpty(t *testing.T) {
	b := &Beat{
		sounds: []*sound.Sound{},
		hash:   map[*sound.Sound]int{},
		mixed:  "",
	}
	expected := "_"
	b.preProcessBeat()
	if expected != b.mixed {
		t.Fatalf("\nFAIL: empty.preProcess()\nExpected:%s\nGot:%s", expected, b.mixed)
	}
}

func TestPreProcess_Beat_WhenOne(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	b := &Beat{
		sounds: []*sound.Sound{snd1},
		hash:   map[*sound.Sound]int{snd1: 0},
		mixed:  "",
	}
	expected := "crash"
	b.preProcessBeat()
	if expected != b.mixed {
		t.Fatalf("\nFAIL: one.preProcess()\nExpected:%s\nGot:%s", expected, b.mixed)
	}
}
func TestPreProcess_Beat_WhenMany(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	b := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2, snd4: 3},
		mixed:  "",
	}
	expected := "crashcrash2crash3crash4"
	b.preProcessBeat()
	if expected != b.mixed {
		t.Fatalf("\nFAIL: many.preProcess()\nExpected:%s\nGot:%s", expected, b.mixed)
	}
}

func TestGet_Kth_WhenNegative(t *testing.T) {
	b := &Beat{
		sounds: []*sound.Sound{},
		hash:   map[*sound.Sound]int{},
		mixed:  "_",
	}
	var expected *sound.Sound
	actual := b.Get(-1)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: Get(-1)\nExpected:%v\nGot:%v", expected, actual)
	}
}

func TestGet_Kth_WhenMany(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	b := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2, snd4: 3},
		mixed:  "",
	}
	actual := b.Get(1)
	if !reflect.DeepEqual(actual, snd2) {
		t.Fatalf("\nFAIL: Get(1)\nExpected:%v\nGot:%v", snd2, actual)
	}
}

func TestGet_Kth_WhenOutOfRange(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	b := &Beat{
		sounds: []*sound.Sound{snd1, snd2, snd3, snd4},
		hash:   map[*sound.Sound]int{snd1: 0, snd2: 1, snd3: 2, snd4: 3},
		mixed:  "",
	}
	var expected *sound.Sound
	actual := b.Get(10)
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: Get(10)\nExpected:%v\nGot:%v", expected, actual)
	}
}
