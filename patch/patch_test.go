package patch

import (
	"drummachinesequencer/patch/beat"
	"drummachinesequencer/patch/sound"
	"os"
	"reflect"
	"testing"
)

func MockFileExistsCB(name string) (os.FileInfo, error) {
	return nil, nil
}

func TestNew_BeatList_Empty(t *testing.T) {
	expected := &Patch{
		beats: make([]*beat.Beat, 0),
		hash:  make(map[*beat.Beat]int),
		curr:  -1,
	}
	actual := New()
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: New()\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestNew_BeatList_Many(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1, snd2)
	beat2 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat2},
		hash:  map[*beat.Beat]int{beat1: 0, beat2: 1},
		curr:  0,
	}
	actual := New(beat1, beat2)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: New()\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestAdd_Beat_ToEmpty(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)

	beat1 := beat.New(snd1, snd2)
	expected := &Patch{
		beats: []*beat.Beat{beat1},
		hash:  map[*beat.Beat]int{beat1: 0},
		curr:  0,
	}
	actual := New()
	actual.Add(beat1)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: ().Add(beat1)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestAdd_Beat_ToOne(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1, snd2)
	beat2 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat2},
		hash:  map[*beat.Beat]int{beat1: 0, beat2: 1},
		curr:  0,
	}
	actual := New(beat1)
	actual.Add(beat2)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1).Add(beat2)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestInsert_Beat_Beginning(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat2, beat1, beat3},
		hash:  map[*beat.Beat]int{beat1: 1, beat2: 0, beat3: 2},
		curr:  1,
	}
	actual := New(beat1, beat3)
	actual.Insert(beat2, 0)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1,beat3).Insert(beat2,0)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestInsert_Beat_Middle(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat2, beat3},
		hash:  map[*beat.Beat]int{beat1: 0, beat2: 1, beat3: 2},
		curr:  0,
	}
	actual := New(beat1, beat3)
	actual.Insert(beat2, 1)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1,beat3).Insert(beat2,1)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestInsert_Beat_OutOfRange(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat3},
		hash:  map[*beat.Beat]int{beat1: 0, beat3: 1},
		curr:  0,
	}
	actual := New(beat1, beat3)
	pos := actual.Insert(beat2, 2)
	expectedPos := -1
	if pos != expectedPos {
		t.Fatalf("\nFAIL: (beat1,beat3).Insert(beat2,2)\nExpected: %d\nGot:%d", expectedPos, pos)
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1,beat3).Insert(beat2,2)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestInsert_Beat_Negative(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat3},
		hash:  map[*beat.Beat]int{beat1: 0, beat3: 1},
		curr:  0,
	}
	actual := New(beat1, beat3)
	pos := actual.Insert(beat2, -1)
	expectedPos := -1
	if pos != expectedPos {
		t.Fatalf("\nFAIL: (beat1,beat3).Insert(beat2,2)\nExpected: %d\nGot:%d", expectedPos, pos)
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1,beat3).Insert(beat2,-1)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestMove_Beat_ToBeginning(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat2, beat1, beat3},
		hash:  map[*beat.Beat]int{beat1: 1, beat2: 0, beat3: 2},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	pos := actual.Move(beat2, 0)
	expectedPos := 0
	if pos != expectedPos {
		t.Fatalf("\nFAIL: (beat1,beat2,beat3).Move(beat2,0)\nExpected: %d\nGot:%d", expectedPos, pos)
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1,beat2,beat3).Move(beat2,0)\nExpected: %v\nGot:%v", expected, actual)
	}

}

func TestMove_Beat_ToEnd(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat3, beat2},
		hash:  map[*beat.Beat]int{beat1: 0, beat2: 2, beat3: 1},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	pos := actual.Move(beat2, 2)
	expectedPos := 0
	if pos != expectedPos {
		t.Fatalf("\nFAIL: (beat1,beat2,beat3).Move(beat2,2)\nExpected: %d\nGot:%d", expectedPos, pos)
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1,beat2,beat3).Move(beat2,2)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestMove_Beat_ToMiddle(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat2, beat1, beat3},
		hash:  map[*beat.Beat]int{beat1: 1, beat2: 0, beat3: 2},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	pos := actual.Move(beat1, 1)
	expectedPos := 0
	if pos != expectedPos {
		t.Fatalf("\nFAIL: (beat1, beat2, beat3).Move(beat1,1)\nExpected: %d\nGot:%d", expectedPos, pos)
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1, beat2, beat3).Move(beat1,1)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestRemove_Beat_FromEmpty(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)

	beat1 := beat.New(snd1)
	expected := &Patch{
		beats: make([]*beat.Beat, 0),
		hash:  make(map[*beat.Beat]int),
		curr:  -1,
	}

	actual := New()
	actual.Remove(beat1)

	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: ().Remove(beat1)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestRemove_Beat_FromBeginning(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat2, beat3},
		hash:  map[*beat.Beat]int{beat2: 0, beat3: 1},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	actual.Remove(beat1)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1, beat2, beat3).Remove(beat1)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestRemove_Beat_FromEnd(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat2},
		hash:  map[*beat.Beat]int{beat1: 0, beat2: 1},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	actual.Remove(beat3)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1, beat2, beat3).Remove(beat3)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestRemove_Beat_FromMiddle(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat3},
		hash:  map[*beat.Beat]int{beat1: 0, beat3: 1},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	actual.Remove(beat2)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1, beat2, beat3).Remove(beat2)\nExpected: %v\nGot:%v", expected, actual)
	}
}

func TestRemove_Beat_Nil(t *testing.T) {
	snd1, _ := sound.New("crash", "fakeaudiopath/crash.wav", "crash sound", MockFileExistsCB)
	snd2, _ := sound.New("crash2", "fakeaudiopath/crash2.wav", "crash sound 2", MockFileExistsCB)
	snd3, _ := sound.New("crash3", "fakeaudiopath/crash3.wav", "crash sound 3", MockFileExistsCB)
	snd4, _ := sound.New("crash4", "fakeaudiopath/crash4.wav", "crash sound 4", MockFileExistsCB)

	beat1 := beat.New(snd1)
	beat2 := beat.New(snd1, snd2)
	beat3 := beat.New(snd3, snd4)
	expected := &Patch{
		beats: []*beat.Beat{beat1, beat2, beat3},
		hash:  map[*beat.Beat]int{beat1: 0, beat2: 1, beat3: 2},
		curr:  0,
	}

	actual := New(beat1, beat2, beat3)
	actual.Remove(nil)
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("\nFAIL: (beat1, beat2, beat3).Remove(Nil)\nExpected: %v\nGot:%v", expected, actual)
	}
}
