package inputformat

import (
	"drummachinesequencer/patch/beat"
	"drummachinesequencer/patch/sound"
	"encoding/json"
	"fmt"
	"log"
	"os"
)

// Adapter object necessary to implement the adapter pattern
type Adapter struct {
	Sounds JSONList
}

// JSONList is a deserialized slice of sound file information
type JSONList []*JSON

// JSON ...
type JSON struct {
	Name        string `json:"name"`
	File        string `json:"file"`
	Description string `json:"description"`
}

// Deserialize your config file here
func Deserialize(path string, reader func(string) ([]byte, error)) (JSONList, error) {
	i := make(JSONList, 0)
	data, err := reader(path)
	if err != nil {
		return nil, fmt.Errorf("failed to load config file at (%s)", path)
	}
	err = json.Unmarshal(data, &i)
	if err != nil {
		return nil, fmt.Errorf("failed to deserialize json data at (%s) with error : %s", path, err.Error())
	}

	return i, nil
}

// ImportJSONSounds is an adapter to support various import formats straight into a sound object
func (j *JSONList) ImportJSONSounds() *beat.Beat {
	soundList := make([]*sound.Sound, 0)
	for _, item := range *j {
		s, err := sound.New(item.Name, item.File, item.Description, os.Stat)
		if err != nil {
			log.Fatalf("Unable to access sound file in config (%s) error : %s", item.Name, err.Error())
		}
		soundList = append(soundList, s)
	}
	return beat.New(soundList...)
}

// ImportSounds is the adapter method that calls the JSON specific Import fuction
func (a *Adapter) ImportSounds() *beat.Beat {
	return a.Sounds.ImportJSONSounds()
}
