package inputformat

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
)

type mockReader func(string) ([]byte, error)

func TestDeserialize_BadFilePath(t *testing.T) {
	input := "./this_path_is_bad.json"
	_, err := Deserialize(input, func(p string) ([]byte, error) { return nil, fmt.Errorf("failed to load config file at (%s)", p) })
	expected := fmt.Sprintf("failed to load config file at (%s)", input)
	if err.Error() != expected {
		t.Fatalf("\nFAIL: Deserialize(%s)\nexpected = %s\ngot %s.", input, expected, err.Error())
	}
}

func TestDeserialize_FailToDeserialize(t *testing.T) {
	input := "./this_path_is_good.json"
	_, err := Deserialize(input, func(p string) ([]byte, error) {
		return []byte(`[{},{}`), nil
	})
	expected := fmt.Errorf("failed to deserialize json data at (%s) with error : %s", input, "unexpected end of JSON input")
	if strings.Compare(err.Error(), expected.Error()) != 0 {
		t.Fatalf("\nFAIL: Deserialize(%s)\nexpected (%s)\ngot (%s)", input, expected.Error(), err.Error())
	}
}

func TestDeserialize_Success(t *testing.T) {
	input := "./this_path_is_good.json"
	description := "correct handling of proper file path and proper json data"
	expected := JSONList{
		{
			Name:        "Song Name",
			File:        "audio/crash.wav",
			Description: "crash sound file",
		},
		{
			Name:        "Song Name 2",
			File:        "audio/crash2.wav",
			Description: "crash 2 sound file",
		},
	}
	actual, err := Deserialize(input, func(p string) ([]byte, error) {
		return []byte(`[{"name":"Song Name","file":"audio/crash.wav","description":"crash sound file"},{"name":"Song Name 2","file":"audio/crash2.wav","description":"crash 2 sound file"}]`), nil
	})
	if err != nil {
		t.Fatalf("\nFAIL: Deserialize(%s)\nexpected = %s\ngot %s.", input, "(nil)", err.Error())
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Fatalf("\nFAIL: %s\nTestDeserialize_Success(%s)\nExpected %v\nActual  %v",
			description, input, expected, actual)
	}
}
