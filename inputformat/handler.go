package inputformat

import "drummachinesequencer/patch/beat"

// Handler is an interface adapter to enable the support of different input formats, i.e. json, toml, xml
// json is currently the only format supported
type Handler interface {
	ImportSounds() *beat.Beat
}

// Client adaptor
type Client struct{}

// LoadDrumkit from the config file
func (c *Client) LoadDrumkit(h Handler) *beat.Beat {
	return h.ImportSounds()
}
